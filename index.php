<!DOCTYPE html>
<html>
<head>
	<title> Lankytinos vietos </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php include "head.php";?>

</head>
<body>

	<?php
    	$parallaxImgMasyvas = array("https://s1.15min.lt/static/cache/OTI1eDYxMCw2NDF4NDAzLDYxNjE3OSxvcmlnaW5hbCwsaWQ9MzQyNDk5MiZkYXRlPTIwMTclMkYxMCUyRjE4LDI0ODI2MjA4MjY=/fotografo-nuotraukose-nuostabi-lietuva-59e75b1cc9c15.jpg", 
    		"https://www.efoto.lt/files/images/54632/IMG_9776-copy.preview.jpg", 
    		"http://geografija.lt/wp-content/gallery/neregeta-lietuva/neregeta-lietuva-292.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-024.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-005.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-116.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-126.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-147.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-157.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-174.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-176.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-190.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-315.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-034.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-048.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-050.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-081.jpg",
    		"https://mokovas.files.wordpress.com/2010/12/neregeta-lietuva-093.jpg",
    		"https://storage.needpix.com/rsynced_images/amusement-2603214_1280.jpg",    		
    		"https://www.efoto.lt/files/images/51001/130907_075454_02.preview.jpg",
    		"https://vignette.wikia.nocookie.net/survivor-malakal/images/4/4a/Neregeta_Lietuva_007.jpg/revision/latest?cb=20150506171256&format=original");
    	$rand_keys = array_rand($parallaxImgMasyvas, 1);
		

	?>
	
		<?php include "header.php";?>
				<div class="parallax-container">
		        <div class="parallax"><img src=<?php echo $parallaxImgMasyvas[$rand_keys]; ?>></div>
		    		
	     		<div class="container top">

			     	<h1 style="text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);">  Ką pažiūrėti </br>Lietuvoje </h1>

					<div id="titulinis-aprasymas" style="font-size: 19px; text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.59); max-width: 50vw;"><p> Seniai buvote išvykę už miesto ribų? Pasiilgote kvapą gniaužiančių vaizdų ir nuostabaus Lietuvos gamtos kraštovaizdžio? O gal norėtumėte pasižvalgyti po istorinį palikimą? Čia tikrai atrasite savo kelionės kryptį savaitgaliui!  </p></div> 
			    	
			    </div>
			</div>


					<div class="intro">
						<div class="container">
							<h4>Kur nueiti, ką pamatyti:</h4>


						<div class="wrapper">
							<div class="row">
								<div class="col s12 m6 l4">
									<div class="col-container">
										<div class="col-content">

				 <div class="slider">
				    <ul class="slides">
				      <li>
				        <img src="images/vilnius.jpg"> <!-- random image -->
				      </li>
				      <li>
				        <img src="images/vilnius1.jpg"> <!-- random image -->
				      </li>
				      <li>
				        <img src="images/vilnius2.jpg"> <!-- random image -->
				      </li>
				      <li>
				        <img src="images/vilnius3.jpg"> <!-- random image -->
				      </li>
				    </ul>
				  </div>

							<a href="https://lt.wikipedia.org/wiki/Vilnius" class="linkas">

							<h6>Vilnius</h6>
							<p> Lietuvos sostinė ir didžiausias šalies miestas</p><br><br>
							<i class="material-icons">arrow_forward</i>
							</a>
						</div>
					</div>	
				</div>

				<div class="col s12 m6 l4">
					<div class="col-content">

					 <div class="slider">
					    <ul class="slides">
					      <li>
					        <img src="images/trakai.jpg" > <!-- random image -->
					      </li>
					      <li>
					        <img src="images/trakai1.jpg" > <!-- random image -->
					      </li>
					      <li>
					        <img src="images/trakai2.jpg" > <!-- random image -->
					      </li>
					      <li>
					        <img src="images/trakai3.jpg" > <!-- random image -->
					      </li>
					    </ul>
					  </div>


						
						<a href="https://lt.wikipedia.org/wiki/Trakai" class="linkas">
						<h6>Trakai</h6>
						<p> Trakai yra Lietuvos turizmo simbolis, panašiai kaip Venecija Italijoje ar Sankt Peterburgas Rusijoje</p>
						<i class="material-icons">arrow_forward</i>
						</a>
					</div>
				</div>

				<div class="col s12 m6 l4">
					<div class="col-content">


					 <div class="slider">
					    <ul class="slides">
					      <li>
					        <img src="images/muziejus.jpg"> <!-- random image -->
					      </li>
					      <li>
					        <img src="images/muziejus1.jpg"> <!-- random image -->
					      </li>
					      <li>
					        <img src="images/muziejus2.jpg"> <!-- random image -->
					      </li>
					      <li>
					        <img src="images/muziejus3.jpg"> <!-- random image -->
					      </li>
					    </ul>
					  </div>

						<!-- <img class="cover" src="images/muziejus.jpg" height="200"> -->
						<a href="https://lt.wikipedia.org/wiki/Lietuvos_j%C5%ABr%C5%B3_muziejus" class="linkas">
						<h6>Klaipeda</h6>
						<p> Jūrų muziejus ir vasaros šventės 760 metų senumo mieste</p><br>
						<i class="material-icons">arrow_forward</i>
						</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<?php include "footer.php";?>

</body>
</html>