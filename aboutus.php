<!DOCTYPE html>
<html>
<head>
	<title> Lankytinos vietos </title>

	<?php include "head.php";?>

</head>
<body>
	<div id="header svoris">
		<?php include "header.php";?>
	</div>


				<div class="parallax-container">
		        <div class="parallax"><img src=images/grazu.jpg></div>	
	     		<div class="container top">
			     	<h1>  Apie mus </h1>
			    </div>
			</div>
		<div class="container">
				<div class="row">

		<div class="col s12">
      <ul class="tabs">
        <li class="tab col s4"><a href="#Gedas">Gedas</a></li>
        <li class="tab col s4"><a href="#Ingrida">Ingrida</a></li>
        <li class="tab col s4"><a href="#Mindaugas">Mindaugas</a></li>
      </ul>
      </div>
  		</div>


		<div id = "Gedas" class="row">
			<div class="col s4">
				<div class="center-align">
					<img class = "img-size" src="images/gedas.jpg">
					<p>Atsakomybės: Programuotojas</p>
					<p>Adresas: Vilnius, Žirmūnų g. 104</p>
					<p>Tel. nr. +370678316**</p>
					<p>El.p. mi.stasevicius@gmail.com</p>
				</div>
			</div>	
		</div>


		<div id = "Ingrida" class="row">

				<div class="col s4 offset-s4">
					<div class="center-align">
						<img class = "img-size" src="images/ingrida.jpg">
						<p>Atsakomybės: Dizainerė</p>
						<p>Adresas: Vilnius, Žirmūnų g. 104</p>
						<p>Tel. nr. +370678316**</p>
						<p>El.p. mi.stasevicius@gmail.com</p>
					</div>
				</div>
		</div>


		<div id = "Mindaugas" class="row">
			<div class="col s4 offset-s8">
			<div class="center-align">
				<img class = "img-size" src="https://scontent.fvno3-1.fna.fbcdn.net/v/t1.0-1/p160x160/11873498_1162785003737289_8839068374760283223_n.jpg?_nc_cat=100&_nc_ht=scontent.fvno3-1.fna&oh=92a17e62aa2b972650dfd6c21581f1d9&oe=5D22368F">
				<p>Atsakomybės: Direktorius</p>
				<p>Adresas: Vilnius, Žirmūnų g. 104</p>
				<p>Tel. nr. +370678316**</p>
				<p>El.p. mi.stasevicius@gmail.com</p>
			</div>
			</div>
		</div>
  </div>



	<div class = "container">

			<div class="row">
				<div class="col s7">
					<div class="left-align">


						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2304.9834238498684!2d25.290128215443474!3d54.7099147794024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd96ae1dedd637%3A0x73978617430c730c!2sUlon%C5%B3+g.+5%2C+Vilnius+08240!5e0!3m2!1slt!2slt!4v1550774640839"  width="500" height="275" frameborder="0" style="border:0" allowfullscreen></iframe>

					</div>
				</div>

				<div class="col s2">
					<div class="left-align">
						<ul>
							<li>Pavadinimas:</li><br>
							<li>Adresas:</li><br>
							<li>Miestas:</li><br>
							<li>Šalis:</li><br>
							<li>Telefonas:</li><br>
							<li>El. paštas:</li><br>
						</ul>
					</div>
				</div>

				<div class="col s3">
					<div class="left-align">
						<ul>
							<li>UAB "Aplankyk visą tėvynę"</li><br>
							<li>Ulonų g. 5</li><br>
							<li>Vilnius</li><br>
							<li>Lietuva</li><br>
							<li>+370678316**</li><br>
							<li>info@aplankyktevyne.lt</li><br>
						</ul>
					</div>
				</div>	

			</div>
	</div>

	<?php include "footer.php";?>

</body>
</html>