<!DOCTYPE html>
<html>
<head>
	<title> Ingrida </title>

	<?php include "head.php";?>

</head>
<body>

	<?php include "header.php";?>

		<div class = "container">
			<h2> Ingrida Paslaptinga </h2>
			<div>
				<img class = "img-size" src="images/ingrida.jpg">
				<p>Atsakomybės: Dizainerė</p>
				<p>Adresas: Vilnius, Žirmūnų g. 104</p>
				<p>Tel. nr. +370678316**</p>
				<p>El.p. mi.stasevicius@gmail.com</p>
			</div>
		</div>

	<?php include "footer.php";?>

</body>
</html>