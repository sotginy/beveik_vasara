<!DOCTYPE html>
<html>
<head>
	<title> Atsitiktinė vieta </title>

	<?php include "head.php";?>

</head>
<body>

	<?php include "header.php";?>

	<?php
    	$parallaxImgMasyvas = array("https://s1.15min.lt/static/cache/OTI1eDYxMCw2NDF4NDAzLDYxNjE3OSxvcmlnaW5hbCwsaWQ9MzQyNDk5MiZkYXRlPTIwMTclMkYxMCUyRjE4LDI0ODI2MjA4MjY=/fotografo-nuotraukose-nuostabi-lietuva-59e75b1cc9c15.jpg", 
    		"https://www.efoto.lt/files/images/54632/IMG_9776-copy.preview.jpg", 
    		"http://geografija.lt/wp-content/gallery/neregeta-lietuva/neregeta-lietuva-292.jpg",
    		"https://storage.needpix.com/rsynced_images/amusement-2603214_1280.jpg",
    		
    		"https://www.efoto.lt/files/images/51001/130907_075454_02.preview.jpg",
    		"https://vignette.wikia.nocookie.net/survivor-malakal/images/4/4a/Neregeta_Lietuva_007.jpg/revision/latest?cb=20150506171256&format=original");
    	$rand_keys = array_rand($parallaxImgMasyvas, 1);
		// print_r( $rand_keys);

	?>
	
				<div class="parallax-container">
		        <div class="parallax"><img src=<?php echo $parallaxImgMasyvas[$rand_keys]; ?>></div>
		        


<div class="atsitiktinisContainer">
		<div class="container">
       <div class="fonasUztamsinimas">
       	
       </div>

       	<div class="row">
<?php
$idArray = array("1", "2", "3", "4", "5", "6", "7","8", "9", "10", "11", "12" );
$rand_keys = array_rand($idArray, 1);
$sql = "SELECT id, pavadinimas, tekstas, nuotrauka, detaliau FROM vietos WHERE id = $idArray[$rand_keys] ";
$result = mysqli_query($conn, $sql);


if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        ?>
        
			     	
			    
          <div class="col s12">

                  
                  <h1 class="top" style="text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);"><?php echo $row["pavadinimas"];?>  </br></h1>

                  <div class="card horizontal">
                    <div class="card-image">
                      <img src='<?php echo $row["nuotrauka"];?>' class = "img-size-news vienas du trys keturi">
                    </div>
                    <div class="card-stacked">
                      <div class="card-content">

                        <div class="row">
                          
                          <div class="col s12">
                            <p><?php echo $row["tekstas"];?></p>
                          </div>

                        </div>

                       
                      </div>
                      <div class="card-action">
                        <a href='<?php echo $row["detaliau"];?>'>Sužinoti daugiau</a>
                      </div>
                    </div>
                </div>    
            </div>


			<?php
			    }
			} else {
			    echo "0 results";
			} ?>
		</div>
		</div>
	</div>
</div>
</div>








	<?php include "footer.php";?>

</body>
</html>