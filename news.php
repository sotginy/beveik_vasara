<!DOCTYPE html>
<html>
<head>
	<title> Naujienos </title>

	<?php include "head.php";?>

</head>
<body>

	<?php include "header.php";?>

      <div class="parallax-container">
            <div class="parallax"><img src=images/lankytina_vieta.jpg></div>  
            <div class="container top">
            <h1 style="text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.3);">Lankytinos</br> vietos</h1>
          </div>
      </div>

		<div class="container">

		<div class="row">
<?php
$sql = "SELECT id, pavadinimas, tekstas, nuotrauka, detaliau FROM vietos";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        ?>
          <div class="col s12 ">
                  <h2 class="header"><?php echo $row["pavadinimas"];?></h2>

                  <div class="card horizontal">
                    <div class="card-image">
                      <img src='<?php echo $row["nuotrauka"];?>' class = "img-size-news vienas du trys keturi">
                    </div>
                    <div class="card-stacked">
                      <div class="card-content">

                        <div class="row">
                          
                          <div class="col s12">
                            <p><?php echo $row["tekstas"];?></p>
                            
                          </div>
                        
                        </div>

                       
                      </div>
                      <div class="card-action">
                         <a href='<?php echo $row["detaliau"];?>'>Sužinoti daugiau</a>
                         </div>
                    </div>
                </div>    
            </div>


<?php
    }
} else {
    echo "0 results";
}

?>
		



 <!--  </div>

  <div class="row">

		<div class="col s12">
    <h2 class="header">Trakai</h2>
    <div class="card horizontal">
      <div class="card-image">
        <img src="images/trakai.jpg" class = "img-size-news vienas du trys keturi">
      </div>
      <div class="card-stacked">
        <div class="card-content">

        	<div class="row">
        		
        		<div class="col s12">
        			<p>Trakų salos pilis – gotikinė pilis Trakų mieste, Galvės ežero Pilies saloje, į kurią nutiestas pėsčiųjų tiltas. Pastatyta kunigaikščių Kęstučio ir Vytauto iniciatyva XIV a. II pusėje – XV a. 1-ajame dešimtmetyje. XV a. ši pilis buvo viena iš LDK valdovų rezidencijų.</p>
        		</div>

        	</div>

         
        </div>
        <div class="card-action">
          <a href="http://www.trakaimuziejus.lt/">Nepakartojami įspūdžiai jūsų laukia čia!</a>
        </div>
      </div>
  </div>    </div>

  </div>


  <div class="row">

		<div class="col s12">
    <h2 class="header">Klaipeda</h2>
    <div class="card horizontal">
      <div class="card-image">
        <img src="images/muziejus.jpg" class = "img-size-news vienas du trys keturi">
      </div>
      <div class="card-stacked">
        <div class="card-content">

        	<div class="row">
        		
        		<div class="col s12">
        			<p>Lietuvos jūrų muziejus – vienas labiausiai lankomų muziejų šalyje, per metus sulaukiantis apie pusę milijono svečių iš Lietuvos, Latvijos, Estijos, Lenkijos, Rusijos, Vokietijos bei kitų užsienio šalių.  Per  beveik 40 metų muziejų aplankė daugiau nei 15 mln. lankytojų. Lietuvos jūrų muziejuje darbas organizuojamas atsižvelgiant į suformuotą viziją, kuri skatina būti muziejui įdomiam visų amžiaus grupių lankytojams.</p>
        		</div>



        	</div>

         
        </div>
        <div class="card-action">
          <a href="https://www.muziejus.lt/">Šios progos nepraleisk ir būtinai apsilankyk!</a>
        </div>
      </div>
  </div>    </div> -->

</div>

  </div>

	<?php include "footer.php";?>

</body>
</html>